<?php
/**
 * Home
 */
$app->get('/', function ($request, $response) {
    $db = $this->db;

//    $testi = $db -> select("m_testimoni.testi AS testim, m_user.nama AS user_nama, m_user.foto AS photo")
//        ->from("m_testimoni")
//        ->join("left join", "m_user", "m_testimoni.m_user_id=m_user.id")
//        ->findAll();

//    $content = $db -> select("m_galeri.foto AS gambar")
//        ->from("m_galeri")
//        ->limit(3)
//        ->findAll();
//    print_r($content); die;
    $jumlahpend = $db -> select("
    COUNT(m_penduduk.id) AS totalAll")
        ->from("m_penduduk")
        ->find();

    $jumlahLk = $db -> select("
    COUNT(m_penduduk.id) AS laki")
        ->from("m_penduduk")
        ->where("jenis_kelamin", "=", "LK")
        ->find();

    $jumlahPr = $db -> select("
    COUNT(m_penduduk.id) AS pr")
        ->from("m_penduduk")
        ->where("jenis_kelamin", "=", "PR")
        ->find();

        // print_r($jumlahpend); die;

    return $this->view->render($response, 'home.twig', [
        'menu' => "home",
//        'testimoni' => $testi,
//        'galeri' => $content,
        'penduduk' => $jumlahpend,
        'cewek' => $jumlahPr,
        'cowok' => $jumlahLk,
        'sosmed' => getSettingweb(),
        'produk' => getProdukHome(),
        'prestasi' => getPrestasiHome(),
//        'artikel' => getAllArtikel(),
        'profile' => getArtikelProfile(),
        'no_wa' => getnoWa(),
        'is_home' => true,
        'seo_title' => getMeta("seo_title"),
        'seo_keywords' => getMeta("seo_keywords"),
        'seo_description' => getMeta("seo_description"),
    ]);
});

/**
 * Pengajuan Surat
 */
$app->get('/pengajuan', function ($request, $response) {
//    $db = $this->db;
//print_die('aaaaaaa');
//    echo json_encode(getAllArtikel());    exit();

//    return $this->view->render($response, 'pengajuan.twig', [
//    ]);
});

/**
 * Acara
 */
$app->get('/acara', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'acara.twig', [
        'menu' => "produk",
        'sosmed' => getSettingweb(),
        'produk' => getProduk(),
        'judul' => "Our Product",
        'header' => "Taman Dhika Sidoarjo",
        'no_wa' => getnoWa(),
        'foto_header' => "slide2.jpg",
        'is_produk' => true,
        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",


    ]);
});

/**
 * Master Plan
 */
$app->get('/product/master-plan.html', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'content/master-plan.twig', [
        'menu' => "master-plan",
        'sosmed' => getSettingweb(),
        'master_plan' => getArtikelMasterPlan(),
        'header' => "Master Plan",
        'judul' => "Taman Dhika Sidoarjo",
        'no_wa' => getnoWa(),
        'foto_header' => "slide2.jpg",
        'is_produk' => true,
        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",


    ]);
});
/**
 * Fasilitas
 */
$app->get('/product/fasilitas.html', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'content/fasilitas.twig', [
        'menu' => "master-plan",
        'sosmed' => getSettingweb(),
        'fasilitas' => getArtikelFasilitas(),
        'header' => "Fasilitas & Keunggulan",
        'judul' => "Taman Dhika Sidoarjo",
        'no_wa' => getnoWa(),
        'foto_header' => "slide2.jpg",
        'is_produk' => true,
        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",


    ]);
});

/**
 * Galley
 */
$app->get('/gallery', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'gallery.twig', [
//        'menu' => "gallery",
//        'sosmed' => getSettingweb(),
//        'produk' => getProduk(),
//        'gallery' => getAllGallery(),
//        'header' => "Gallery",
//        'judul' => "Taman Dhika Sidoarjo",
//        'no_wa' => getnoWa(),
//        'foto_header' => "slide2.jpg",
//        'is_gallery' => true,
//        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
//        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
//        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",


    ]);
});

/**
 * Berita Acara
 */
$app->get('/artikel', function ($request, $response) {

    $db = $this->db;
    $models = $db->select("artikel.*,
    artikel_kategori.nama AS kategori,
    m_user.nama AS publisher")
        ->from("artikel")
        ->join("left join", "artikel_kategori", "artikel.kategori=artikel_kategori.id")
        ->join("left join","m_user","m_user.id = artikel.created_by")
        ->findAll();

    foreach ($models as $key => $val){
        $models[$key] = (array) $val;
        $models[$key]['gambar_thumb'] = gambar_pertama($val->isi_content);
    }

    $kategori = $db -> select("artikel_kategori.nama AS kategori")
    ->from("artikel_kategori")
    ->groupby("artikel_kategori.id")
    ->findAll();

//    $cari = $db -> select("artikel.judul AS judul_artikel, artikel_kategori.nama AS kategori_artikel")
//        ->from("artikel")
//        ->join("left join", "artikel_kategori", "artikel.kategori=artikel_kategori.id");

    return $this->view->render($response, 'list_blog.twig', [
        "artikel" => $models,
        "category" => $kategori,
        "artikel_terbaru" => getAllArtikel(),
    ]);
});


/**
 * Berita Acara Detail
 */
$app->get('/artikel/{alias}', function ($request, $response) {
    $params = $request->getAttribute("alias");

    $db = $this->db;
    $content = $db->select("artikel.*,m_user.nama as penulis, artikel_kategori.nama AS kategori")
        ->from("artikel")
        ->join("left join", "m_user", "m_user.id = artikel.created_by")
        ->join("left join", "artikel_kategori", "artikel.kategori=artikel_kategori.id")
        ->where("status", "=", "publish")
        ->andWhere("artikel.alias", '=', $params)
        ->orderBy("artikel.id DESC")
        ->find();
//    foreach ($content as $key => $value) {
    $content->judultitle = $content->judul;
    $content->jam = date('H:i', $content->jam);
    $content->gambar_thumb = gambar_pertama($content->isi_content);
    $content->created_at = date('d F Y', $content->created_at);
    $content->tgl_kontrak = date('d F Y', strtotime($content->tgl_kontrak));
//    }

    $kategori = $db -> select("artikel_kategori.nama AS kategori")
        ->from("artikel_kategori")
        ->groupby("artikel_kategori.id")
        ->findAll();

//    echo json_encode(getNextArtikel($content->id));exit();
    return $this->view->render($response, 'blog_detail.twig', [
        'menu' => "portofolio",
        'sosmed' => getSettingweb(),
        'artikel' => $content,
        'category' => $kategori,
        'artikel_terbaru' => getAllArtikel(),
        'header' => "Berita & Acara ",
        'judul' => $content->judul,
        'no_wa' => getnoWa(),
        'foto_header' => "slide2.jpg",
        'is_berita' => true,
        'seo_title' => $content->judul,
        'seo_keywords' => getMeta("seo_keywords"),
        'seo_description' => " $content->deskripsi",

    ]);
});


/**
 * Kontak Kami
 */
$app->get('/contact', function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    return $this->view->render($response, 'contact.twig', [
        'menu' => "contact",
    ]);
});

/**
 * History
 */
$app->get('/history', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'history.twig', [
//        'menu' => "kontak-kami",
//        'sosmed' => getSettingweb(),
//        'header' => "Contact Us",
//        'no_wa' => getnoWa(),
//        'judul' => "Stay With Us",
//        'foto_header' => "contact-us2.jpg",
//        'is_kontak' => true,
//        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
//        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
//        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",

    ]);
});

/**
 * INFO!!
 */


/**
 * Keluhan Pelanggan
 */
$app->get('/info/keluhan-pelanggan.html', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'content/keluhan-pelanggan.twig', [
        'sosmed' => getSettingweb(),
        'header' => "Keluhan Pelanggan",
        'no_wa' => getnoWa(),
        'judul' => "Taman Dhika Sidoarjo",
        'foto_header' => "slide2.jpg",
        'is_info' => true,
        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",

    ]);
});


/**
 * Legal Information
 */
$app->get('/info/legal-information.html', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'content/legal-information.twig', [
        'sosmed' => getSettingweb(),
        'artikel' => getAllLegalInformation(),
        'header' => "Legal Information",
        'judul' => "Taman Dhika Sidoarjo",
        'no_wa' => getnoWa(),
        'foto_header' => "slide2.jpg",
        'is_info' => true,
        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",


    ]);
});
/**
 * Legal Information Detail
 */
$app->get('/legal-information/{alias}.html', function ($request, $response) {
    $params = $request->getAttribute("alias");

    $db = $this->db;
    $content = $db->select("*")
        ->from("artikel")
        ->where("status", "=", "publish")
        ->andWhere("artikel.alias", '=', $params)
        ->orderBy("artikel.id DESC")
        ->findAll();
    foreach ($content as $key => $value) {
        $value->judultitle = $value->judul;
        $value->jam = date('H:i', $value->jam);
        $value->gambar_thumb = gambar_pertama($value->isi_content);
        $value->created_at = date('d F Y', $value->created_at);
        $value->tgl_kontrak = date('d F Y', strtotime($value->tgl_kontrak));
    }


    return $this->view->render($response, 'content/legal-information-detail.twig', [
        'menu' => "portofolio",
        'sosmed' => getSettingweb(),
        'artikel' => $content,
        'next_artikel' => getNextLegalInformation($value->id),
        'header' => "Berita & Acara ",
        'judul' => "Taman Dhika Sidoarjo",
        'no_wa' => getnoWa(),
        'foto_header' => "slide2.jpg",
        'is_berita' => true,
        'seo_title' => " $value->judul",
        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
        'seo_description' => " $value->deskripsi",

    ]);
});


/**
 * About
 */
$app->get('/about', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'about.twig', [
        'menu' => "tentang-kami",
        'sosmed' => getSettingweb(),
        'profile' => getArtikelProfile(),
        'galeri' => getSlider(),
        'produk' => getProduk(),
        'header' => "Tentang Kami",
        'no_wa' => getnoWa(),
        'judul' => "Taman Dhika Sidoarjo",
        'foto_header' => "slide2.jpg",
        'is_about' => true,
        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",


    ]);
});
/**
 * Produk Detail
 */
//$app->get('/{nama}.html', function ($request, $response) {
//    $params = $request->getAttribute("nama");
//    $db = $this->db;
//
//
//    return $this->view->render($response, 'content/product-detail.twig', [
//        'sosmed' => getSettingweb(),
//        'productDetail' => getProductDetail($params),
//        'judul' => getProductDetailNama($params),
//        'no_wa' => getnoWa(),
//        'header' => "Taman Dhika Sidoarjo",
//        'foto_header' => "slide2.jpg",
//        'is_produk' => true,
//        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
//        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
//        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",
//
//    ]);
//});


/**
 * Send Whatsapp
 */
$app->post('/kirim-wa', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $model = $db->select("*")
        ->from("setting")
        ->find();
    $nama = $params['nama'];
    $pesan = $params['pesan'];
    $email = $params['email'];


    $to = $model->email;
    $subject = $nama;
    $txt = $pesan;
    $txt = " Email dari " . $email . " dengan Nama : " . $nama . " Pesan " . $pesan;

    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->SMTPDebug = 0;

    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = "noreplyinfosystems@gmail.com";
    $mail->Password = "bismillah2018";
    /*$mail->Username = $getEmail->email_smtp;
    $mail->Password = $getEmail->password_smtp;*/
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->setFrom('info@landa.co.id', "Taman Dhika Sidoarjo");
    $mail->addAddress($to, "$to");
    // foreach ($email_penerima as $key => $value) {
    // $mail->addAddress($value);
    // }

    $mail->isHTML(true);

    $mail->Subject = $subject;
    $mail->Body = $txt;

    if (!$mail->send()) {
        return $this->view->render($response, 'content/kontak-kami.twig', [
            'menu' => "kontak-kami",
            'sosmed' => getSettingweb(),
            'judul' => "Stay With Us",
            'header' => "Kontak Kami",
            'balasan' => 'Gagal',
            'foto_header' => "contact-us2.jpg",
            'is_kontak' => true,
            'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
            'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
            'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",
            'pesan' => "gagal"


        ]);
    } else {
        return $this->view->render($response, 'content/kontak-kami.twig', [
            'menu' => "kontak-kami",
            'sosmed' => getSettingweb(),
            'header' => "Kontak Kami",
            'judul' => "Stay With Us",
            'balasan' => 'Berhasil',
            'foto_header' => "contact-us2.jpg",
            'is_kontak' => true,
            'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
            'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
            'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",
            'pesan' => "berhasil"

        ]);
    }
    /*} else {
        return [
            'status' => false,
            'error' => 'Email SMTP Belum di setting',
        ];
    }*/

});


/**
 * Send Keluhan
 */
$app->post('/kirim-keluhan', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $model = $db->select("*")
        ->from("setting")
        ->find();


    $keluhan = $db->insert("m_keluhan", $params);


//    echo json_encode($keluhan);
//    die();
    $nama = $params['nama'];
    $judul = $params['subject'];
    $email = $params['email'];
    $keluhan_detail = $params['description'];
    $blok = $params['blok'];
    $unit = $params['unit'];


    $to = $model->email;
    $subject = "Keluhan Pelanggan";
    $txt = " Email dari " . $email . " dengan Nama : " . $nama . " ,judul Keluhan  : " . $judul . " ,Keluhan  : " . $keluhan_detail . " ,di Blok : " . $blok . " ,Unit : " . $unit;

    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->SMTPDebug = 0;

    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = "noreplyinfosystems@gmail.com";
    $mail->Password = "bismillah2018";
    /*$mail->Username = $getEmail->email_smtp;
    $mail->Password = $getEmail->password_smtp;*/
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->setFrom('info@landa.co.id', "Taman Dhika Sidoarjo");
    $mail->addAddress($to, "$to");
    // foreach ($email_penerima as $key => $value) {
    // $mail->addAddress($value);
    // }

    $mail->isHTML(true);

    $mail->Subject = $subject;
    $mail->Body = $txt;

    if (!$mail->send()) {

        return $this->view->render($response, 'content/keluhan-pelanggan.twig', [
            'sosmed' => getSettingweb(),
            'header' => "Keluhan Pelanggan",
            'no_wa' => getnoWa(),
            'judul' => "Taman Dhika Sidoarjo",
            'foto_header' => "slide2.jpg",
            'is_info' => true,
            'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
            'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
            'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",
            'pesan' => "gagal"

        ]);
    } else {
        return $this->view->render($response, 'content/keluhan-pelanggan.twig', [
            'sosmed' => getSettingweb(),
            'header' => "Keluhan Pelanggan",
            'no_wa' => getnoWa(),
            'judul' => "Taman Dhika Sidoarjo",
            'foto_header' => "slide2.jpg",
            'is_info' => true,
            'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
            'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
            'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",
            'pesan' => "berhasil"

        ]);
    }
    /*} else {
        return [
            'status' => false,
            'error' => 'Email SMTP Belum di setting',
        ];
    }*/

});